# node management

## Datagrid

- REPO: https://github.com/adazzle/react-data-grid#readme
- EXAMPLE: https://adazzle.github.io/react-data-grid/canary/?path=/story/demos--common-features

## Node managers

**vis**

- REPO: https://github.com/visjs/vis-network
- EXAMPLE: https://visjs.github.io/vis-network/examples/

**graphdracula**

- REPO: https://github.com/strathausen/dracula
- EXAMPLE: https://www.graphdracula.net/showcase/

**gojs**

- REPO: https://github.com/NorthwoodsSoftware/GoJS
- EXAMPLE: https://gojs.net/latest/samples/dataFlow.html

**GGEditor**

- REPO: https://github.com/alibaba/GGEditor
- EXAMPLE: https://ggeditor.com/guide/getting-started
- když jsem začal dělat složitější věci a chtěl jsem se kouknout na issuse, tak je to pain... https://github.com/alibaba/GGEditor/issues
- problematické chování, když místo labelu chci renderovat v node nějaké custom html

**react-flow-editor**

- REPO: https://github.com/lochbrunner/react-flow-editor
- EXAMPLE: https://github.com/lochbrunner/react-flow-editor/blob/master/docs/screen.gif
- spousta typescriptu, se kterým nevím, jak pracovat

**react-digraph**

- REPO: https://github.com/uber/react-digraph
- EXAMPLE: https://raw.githubusercontent.com/uber/react-digraph/master/example.gif
