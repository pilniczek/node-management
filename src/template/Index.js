import React from "react"
import loadable from '@loadable/component'
import Layout from "../layouts"

// eslint-disable-next-line
const ClientSideComponent = loadable(() => import ("../components/Editor"))

const Index = () => {
	return (
		<Layout>
			<ClientSideComponent />
		</Layout>
	)
}

export default Index