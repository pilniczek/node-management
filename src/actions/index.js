
export const viewportDimensionAction = store => {
	const isClient = typeof window === "object"
	const newValue = {
		width: isClient ? window.innerWidth : 0,
		height: isClient ? window.innerHeight : 0,
	}
	store.setState({ windowDimensions: newValue })
}

export const none = () => (null)
 