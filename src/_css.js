import { createGlobalStyle } from "styled-components"

const globalStyles = createGlobalStyle`
*,
*:before,
*:after {
	box-sizing: border-box;
}
html {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
	font-smoothing: antialiased;
	text-shadow: rgba(0, 0, 0, 0.01) 0 0 1px;
	font-family: "Oswald", open sans,sans-serif /*"Quicksand", Arial, Helvetica, sans-serif*/;
	font-size: 62.5%;
	overflow: hidden;
	font-weight: 300;
}
body {
	font-size: 1.8rem;
	line-height: 1.618;
	overflow-y: scroll;
}
html,
body {
	min-height: 100%;
	height: 100%;
	margin: 0;
	padding: 0;
	overflow-x: hidden;
}
#___gatsby,
#gatsby-focus-wrapper {
	min-height: 100%;
	height: 100%;
	width: 100%;
}
article,
aside,
details,
figcaption,
figure,
footer,
header,
main,
menu,
nav,
section,
summary {
	display: block;
}
audio,
canvas,
progress,
video {
	display: inline-block;
}
audio:not([controls]) {
	display: none;
	height: 0;
}
progress {
	vertical-align: baseline;
}
[hidden],
template {
	display: none;
}
a {
	background-color: transparent;
	-webkit-text-decoration-skip: objects;
	text-decoration: none;
	color: unset;
	position: relative;
	display: inline-block;
	cursor: pointer;
	&:after, &:before {
		content: "";
		display: block;
		width: 100%;
		position: absolute;
		bottom: 0;
		left: 0;
		border-bottom: 1px solid silver;
		transition: bottom .2s;
	}
	&:hover, &:focus {
		outline: none;
		&:before {
			bottom: 100%;
		}
	}
}
button {
	background-color: transparent;
	text-decoration: none;
	border: none;
	cursor: pointer;
}
a:active,
a:hover,
button:active,
button:hover {
	outline-width: 0;
}
abbr[title] {
	border-bottom: none;
	text-decoration: underline;
	text-decoration: underline dotted;
}
b,
strong {
	font-weight: 500;
}
dfn {
	font-style: italic;
}
mark {
}
small {
	font-size: 80%;
}
sub,
sup {
	font-size: 75%;
	line-height: 0;
	position: relative;
	vertical-align: baseline;
}
sub {
	bottom: -0.25em;
}
sup {
	top: -0.5em;
}
img {
	border-style: none;
	max-width: 100%;
}
svg:not(:root) {
	overflow: hidden;
}
code,
kbd,
pre,
samp {
	font-family: monospace, monospace;
	font-size: 90%;
}
figure {
	margin: 1em 40px;
}
hr {
	height: 0;
	overflow: hidden;
	margin: 0 0 2rem 0;
	padding: 0 0 2rem 0;
	border: 0;
}
button,
input,
optgroup,
select,
textarea {
	font: inherit;
	margin: 0;
	padding: 5px 1.5rem;
	display: block;
}
input,
textarea {
}
optgroup {
	font-weight: 500;
}
button,
input {
	overflow: visible;
}
button,
select {
	text-transform: none;
}
[type="reset"],
[type="submit"],
button,
html [type="button"] {
	-webkit-appearance: button;
}
[type="button"]::-moz-focus-inner,
[type="reset"]::-moz-focus-inner,
[type="submit"]::-moz-focus-inner,
button::-moz-focus-inner {
	border-style: none;
	padding: 0;
}
[type="button"]:-moz-focusring,
[type="reset"]:-moz-focusring,
[type="submit"]:-moz-focusring,
button:-moz-focusring {
}
fieldset {
	margin: 0 2px;
	padding: 0.35em 0.625em 0.75em;
}
legend {
	box-sizing: border-box;
	color: inherit;
	display: table;
	max-width: 100%;
	padding: 0;
	white-space: normal;
}
textarea {
	overflow: auto;
}
[type="checkbox"],
[type="radio"] {
	box-sizing: border-box;
	padding: 0;
}
[type="number"]::-webkit-inner-spin-button,
[type="number"]::-webkit-outer-spin-button {
	height: auto;
}
[type="search"] {
	-webkit-appearance: textfield;
	outline-offset: -2px;
}
[type="search"]::-webkit-search-cancel-button,
[type="search"]::-webkit-search-decoration {
	-webkit-appearance: none;
}
::-webkit-input-placeholder {
	color: inherit;
	opacity: 0.54;
}
::-webkit-file-upload-button {
	-webkit-appearance: button;
	font: inherit;
}
`

export default globalStyles
