import styled from "styled-components"

const StyledLayout = styled.div`
	height: 100%;
	.app {
		min-height: 100%;
		width: 100%;
		position: relative;
		display: flex;
		flex-direction: row;
	}
	.app__body {
		display: flex;
		flex-direction: column;
		justify-content: space-between;
		flex: 1 100%;
		order: 1;
		overflow: visible;
		width: 100%;
		height: 100%;
	}
	.app__content {
		overflow: visible;
		height: 100%;
	}
	.content__blog {
		max-width: 800px;
		margin: auto;
	}
`

export default StyledLayout
