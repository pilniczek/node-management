import React, { useEffect } from "react"
import { node } from "prop-types"
import StyledLayout from "./style"
import useGlobal from "../store"
import GlobalStyle from "../_css"

const Layout = ({ children }) => {
	const [_set, globalActions] = useGlobal()

	useEffect(() => {
		function handleResize() {
			globalActions.viewportDimensionAction()
		}
		handleResize()
		window.addEventListener("resize", handleResize)
		return () => window.removeEventListener("resize", handleResize)
	}, []) // Empty array ensures that effect is only run on mount and unmount

	return (
		<>
			<GlobalStyle />
			<StyledLayout>
				<div className="app__body">
					<div className="app__content" role="main">{children}</div>
				</div>
			</StyledLayout>
		</>
	)
}

Layout.propTypes = {
	children: node.isRequired,
}

export default Layout
