import React from "react"
import GGEditor, { Flow, Command, ContextMenu, constants, RegisterNode/* , EditableLabel */ } from "gg-editor"
import StyledEditor from "./style"

/* const NodeContent = () => (
	<div className="node-content">
		<table>
			<tr>
				<td>head1</td>
				<td>head2</td>
			</tr>
			<tr>
				<td>val1</td>
				<td>val2</td>
			</tr>
		</table>
	</div>
) */

const { EditorCommand } = constants

const data = {
	nodes: [
		{
			id: '0',
			label: 'Node',
			x: 25,
			y: 25,
		},
		{
			id: '1',
			label: 'Node',
			x: 375,
			y: 75,
		},
	],
	edges: [
		{
			label: 'Label',
			source: '0',
			sourceAnchor: 1,
			target: '1',
			targetAnchor: 0,
		},
	],
}

const FLOW_COMMAND_LIST = [
	EditorCommand.Undo,
	EditorCommand.Redo,
	EditorCommand.Copy,
	EditorCommand.Paste,
	EditorCommand.Remove,
	EditorCommand.ZoomIn,
	EditorCommand.ZoomOut,
]

const Editor = () => {
	return (
		<StyledEditor>
			<GGEditor className="GGEditor">
				<div className="toolbar">
					{FLOW_COMMAND_LIST.map((name) => {
						return (
							<Command key={name} name={name}>
								{name}
							</Command>
						)
					})}
				</div>
				<Flow 
					className="editorBd"
					style={{height:"100%"}}
					data={data}
					graphConfig={{ defaultNode: { type: 'customNode' } }/* { renderer: 'svg', defaultNode: { type: 'customDomNode' }, defaultEdge: { type: 'polyline' } } */}
				/>
				<RegisterNode
					name="customNode"
					config={{
						getCustomConfig() {
							return {
								wrapperStyle: {
									fill: '#5E6A7D',
								},
								contentStyle: {
									fill: '#5E6A7D',
								},
								labelStyle: {
									fill: '#FFFFFF',
								},
							}
						},
					}}
					extend="bizFlowNode"
				/>
				<RegisterNode
					name="customDomNode"
					config={{
						draw(model, group) {
							const keyShape = group.addShape('rect', {
								draggable: true,
								attrs: {
									x: 0,
									y: 0,
									width: 250,
									height: 250,
									fill: '#5487ea',
									radius: 4,
								},
							})
							group.addShape('dom', {
								attrs: {
									x: 0,
									y: 0,
									width: 250,
									height: 250,
									html: `
									<div style="user-select: none;">
										<table>
											<tr>
												<td>head1</td>
												<td>head2</td>
											</tr>
											<tr>
												<td>val1</td>
												<td>val2</td>
											</tr>
										</table>
									</div>`,
								},
							})
							return keyShape
						},
					}}
					extend="single-shape"
				/>
				<ContextMenu
					renderContent={(item, position, hide) => {
						const { x: left, y: top } = position
						return (
							<div className="context-menu" style={{ top, left }}>
								{[EditorCommand.Undo, EditorCommand.Redo, EditorCommand.PasteHere].map(name => {
									return (
										<button type="button" onClick={hide} className="button">
											<Command key={name} name={name}>
												{name}
											</Command>
										</button>
									)
								})}
							</div>
						)
					}}
				/>
			</GGEditor>
		</StyledEditor>
	)
}

export default Editor
