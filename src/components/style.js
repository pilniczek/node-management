import styled from "styled-components"

const StyledEditor = styled.div`
height: 100%;
position: relative;
padding-top: 54px;
.GGEditor {
  height: 100%;
}
.graph {
  position: relative;
}
.toolbar {
  display: flex;
  align-items: center;
  padding: 8px;
  border-bottom: 1px solid #e8e8e8;
  background: #ffffff;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  height: 54px;
}
.command {
  display: inline-block;
  margin: 0 6px;
  padding-top: 6px;
  text-align: center;
  border: 1px solid #fff;
  user-select: none;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
}
.command-disabled {
  color: rgba(0, 0, 0, 0.25);
  cursor: default;
  &:hover {
    text-decoration: none;
  }
}
.context-menu {
  position: absolute;
  background: #ffffff;
  border-radius: 4px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
  overflow: hidden;
  .command {
    display: block;
  }
  .button {
    padding: 0;
    margin: 0;
    border: none;
  }
}
`

export default StyledEditor
