module.exports = {
	pathPrefix: process.env.PATH_PREFIX || "/",
	siteMetadata: {
		author: `Tomas Pilnaj`,
		title: `Tomas Pilnaj`,
		description: `Vývojář webových aplikací a kodér HTML e-mailů.`,
		siteUrl: "https://tomas-pilnaj.cz",
	},
	plugins: [
		{
			resolve: "gatsby-plugin-eslint",
			options: {
				exclude: /(node_modules|.cache|public)/,
				stages: ["develop"],
				options: {
					failOnError: true,
				},
			},
		},
		`gatsby-plugin-styled-components`,
		{
			resolve: `gatsby-plugin-layout`,
			options: {
				component: require.resolve(`./src/template/Index.js`),
			},
		},
	],
}
